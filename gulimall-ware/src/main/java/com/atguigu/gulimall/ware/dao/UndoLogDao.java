package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author pirancle
 * @email 1435200537@qq.com
 * @date 2023-09-21 20:52:10
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
