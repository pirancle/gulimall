package com.atguigu.gulimall.product.vo;

import lombok.Data;

@Data
public class AttrRespVo extends AttrVo{
    String groupName;
    String catelogName;
    Long[] catelogPath;
}
